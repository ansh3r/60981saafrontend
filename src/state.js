import {createStore} from "vuex";
import router from "./router";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const client_id = process.env.VUE_APP_CLIENT_ID;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;

const store = createStore({
    state: {
        user: {},
        token: null,
        preLoading: false,
        dataPreLoading: true,
        loginError: false,
        loggedIn: false,
        passer: {},
        pager: {
            currentPage: 1,
            perPage: 5,
        },
        validation: {},
        createRatingDialogVisible: false,
    },
    
    mutations: {
        setToken(state, token) {
            state.token = token;
        },

        getToken(context, token) {
            context.token = token;
        },

        setUser(context, user) {
            context.user = user;
        },

        setPreloading(context, is_load) {
            context.preLoading = is_load;
        },
        setDataPreloading(context, is_load) {
            context.dataPreLoading = is_load;
        },
        setLoginError(context, isError) {
            context.loginError = isError
        },
        setLoggedIn(context, isLoggedIn) {
            context.loggedIn = isLoggedIn
        },
        setPasser(context, passer) {
            context.passer = passer
        },
        setPage(context, page) {
            context.pager.currentPage = page
        },
        setPager(context, pager) {
            context.pager = pager
        },
        setPerPage(context, rows) {
            context.pager.perPage = rows
        },
        setValidation(context, validation) {
            context.validation = validation;
        },
        setCreatePassDialogVisible(context, visible) {
            context.createPassDialogVisible = visible;
        },
        logout(context) {

            context.user = null;
            context.token = null;
            context.loggedIn = false;
            context.passer = {};
            localStorage.removeItem('token')
            router.push('/')
        },
    },
    actions: {
        auth(context, {login, password}) {

            context.commit('setPreloading', true)
            window.axios.post(backendUrl + '/OAuthController/Authorize', {
                username: login,
                password: password,
                grant_type: 'password'
            }, {
                headers: {
                    Authorization: 'Basic ' + window.btoa(client_id + ':' + client_secret)
                }
            }).then((response) => {


                let arr = []
                for(let i of response.data) {
                    arr.push(i)
                    if (i == '}') {
                        break;
                    }
                }
                let str = arr.join('');

                if (JSON.parse(str).access_token) {
                    context.commit('setToken', JSON.parse(str).access_token)
                    localStorage.setItem('token', JSON.parse(str).access_token);
                    context.commit('setLoggedIn', true);
                    context.commit('setLoginError', false);
                    context.dispatch('getUser');
                } else {
                    context.commit('setLoginError', true)
                    context.commit('setPreloading', false)
                }
            })

        },
        getUser(context) {
            context.commit('setPreloading', true);
            console.log('get user')
            return window.axios.get(backendUrl + '/OAuthController/user', {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            }).then((response) => {

                let arr = []
                for(let i of response.data) {
                    arr.push(i)
                    if (i == '}') {
                        break;
                    }
                }
                let str = arr.join('');

                context.commit('setUser', JSON.parse(str));
                context.commit('setPreloading', false);

            }).catch(error => {
                    if (error.response) context.commit('setLoggedIn', false);
                }
            )

        },
        getPass(context) {
            console.log('get pass')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('per_page', context.state.pager.perPage)
            return window.axios.post(backendUrl + '/PasserApi/passer?page_group1=' + context.state.pager.currentPage, params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {

                context.commit('setPasser', response.data.passwords);
                context.commit('setPager', response.data.pager);
                context.commit('setDataPreloading', false)
                console.log(response.data.passwords)
                console.log(response.data.pager)

            })
        },
        createPass(context, {data_login, data_password, sitename, date}) {
            console.log('create pass')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('data_login', data_login)
            params.append('data_password', data_password)
            params.append('sitename', sitename)
            params.append('date', date)
            return window.axios.post(backendUrl + '/PasserApi/store', params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 201) {
                    console.log("Pass created successfully");
                    context.commit("setValidation", {})
                    context.commit('setCreatePassDialogVisible', false);
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getPass');
                    router.push('/passer');
                } else {
                    console.log(response.data)
                    context.commit("setValidation", response.data)
                }
            })
        },

        deletePass(context, {id}) {
            console.log('delete pass')
            context.commit('setDataPreloading', true)
            return window.axios.get(backendUrl + '/PasserApi/delete/' + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 200) {
                    console.log("Pass deleted successfully");
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getPass');
                    router.push('/passer');
                } else {
                    console.log(response.data)
                }
            })
        }
    }
})

export default store;